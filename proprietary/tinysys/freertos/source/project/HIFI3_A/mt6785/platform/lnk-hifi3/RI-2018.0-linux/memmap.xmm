//  Memory map file to generate linker scripts for programs run on the ISS.

// Customer ID=7578; Build=0x7aa7f; Copyright (c) 2004-2015 Cadence Design Systems, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


// A memory map is a sequence of memory descriptions and
// optional parameter assignments.
//
// Each memory description has the following format:
//   BEGIN <name>
//     <addr> [,<paddr>] : <mem-type> : <mem-name> : <size> [,<psize>]
//                       : [writable] [,executable] [,device] ;
//     <segment>*
//   END <name>
//
// where each <segment> description has the following format:
//     <seg-name> : F|C : <start-addr> - <end-addr> [ : STACK ] [ : HEAP ]
//                : <section-name>* ;
//
// Each parameter assignment is a keyword/value pair in the following format:
//   <keyword> = <value>                (no spaces in <value>)
// or
//   <keyword> = "<value>"              (spaces allowed in <value>)
//
// The following primitives are also defined:
//   PLACE SECTIONS( <section-name>* ) { WITH_SECTION(<section-name>)
//                                       | IN_SEGMENT(<seg-name>) }
//
//   NOLOAD <section-name1> [ <section-name2> ... ]
//
// Please refer to the Xtensa LSP Reference Manual for more details.
//

BEGIN dram0
0x4ffb0000: dataRam : dram0 : 0x10000 : writable ;
 dram_0 : C : 0x4ffb0000 - 0x4ffb77ff :  STACK : .UserExceptionVector.literal .ResetVector.literal .NMIExceptionVector.literal .Level4InterruptVector.literal .Level3InterruptVector.literal .Level2InterruptVector.literal .KernelExceptionVector.literal .DoubleExceptionVector.literal .DebugExceptionVector.literal .dram.rodata .dram.literal .dram.data .dram.bss;
 dram_reserved : F : 0x4ffb7800 - 0x4ffb7fff : .dram_reserved;
END dram0
BEGIN dram1
0x4ffc0000: dataRam : dram1 : 0x20000 : writable ;
END dram1
BEGIN iram0
0x4ffe0000: instRam : iram0 : 0x10000 : executable, writable ;
 iram0_1 : F : 0x4ffe0400 - 0x4ffe057b : .WindowVectors.text;
 iram0_2 : F : 0x4ffe057c - 0x4ffe059f : .Level2InterruptVector.text;
 iram0_3 : F : 0x4ffe05a0 - 0x4ffe05df : .Level3InterruptVector.text;
 iram0_4 : F : 0x4ffe05e0 - 0x4ffe061f : .Level4InterruptVector.text;
 iram0_5 : F : 0x4ffe0620 - 0x4ffe065f : .DebugExceptionVector.text;
 iram0_6 : F : 0x4ffe0660 - 0x4ffe069b : .NMIExceptionVector.text;
 iram0_7 : F : 0x4ffe069c - 0x4ffe06bb : .KernelExceptionVector.text;
 iram0_8 : F : 0x4ffe06bc - 0x4ffe06db : .UserExceptionVector.text;
 iram0_9 : F : 0x4ffe06dc - 0x4ffe06fb : .DoubleExceptionVector.text;
 iram0_10 : C : 0x4ffe06fc - 0x4ffe8fff : .iram.literal .iram.text;
 iram0_0 : F : 0x4ffe0000 - 0x4ffe03ff : .ResetVector.text .ResetHandler.literal .ResetHandler.text;
END iram0
BEGIN sram
0x56000000: sysram : sram : 0x700000 : executable, writable ;
 sram_0 : C : 0x56000400 - 0x566fffff :  HEAP : .sram.text .text .sram.literal .literal .clib.rodata .rtos.rodata .sram.rodata .rodata .mpu_ro_separator .clib.percpu.data .rtos.percpu.data .clib.data .rtos.data .data .clib.percpu.bss .rtos.percpu.bss .clib.bss .rtos.bss .bss;
END sram
BEGIN confreg
0x0: sysram : confreg : 0x40000000 : writable, device ;
END confreg
BEGIN audiosys
0xd1210000: sysram : audiosys : 0x1000 : writable, device ;
END audiosys
VECSELECT=0