/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

.MFNR = {
    imx499mipiraw_MFNR_0000, imx499mipiraw_MFNR_0001, imx499mipiraw_MFNR_0002, imx499mipiraw_MFNR_0003, imx499mipiraw_MFNR_0004, imx499mipiraw_MFNR_0005, imx499mipiraw_MFNR_0006, imx499mipiraw_MFNR_0007, imx499mipiraw_MFNR_0008, imx499mipiraw_MFNR_0009,
},
.SWNR = {
    imx499mipiraw_SWNR_0000, imx499mipiraw_SWNR_0001, imx499mipiraw_SWNR_0002, imx499mipiraw_SWNR_0003, imx499mipiraw_SWNR_0004, imx499mipiraw_SWNR_0005, imx499mipiraw_SWNR_0006, imx499mipiraw_SWNR_0007, imx499mipiraw_SWNR_0008, imx499mipiraw_SWNR_0009,
    imx499mipiraw_SWNR_0010, imx499mipiraw_SWNR_0011, imx499mipiraw_SWNR_0012, imx499mipiraw_SWNR_0013, imx499mipiraw_SWNR_0014, imx499mipiraw_SWNR_0015, imx499mipiraw_SWNR_0016, imx499mipiraw_SWNR_0017, imx499mipiraw_SWNR_0018, imx499mipiraw_SWNR_0019,
    imx499mipiraw_SWNR_0020, imx499mipiraw_SWNR_0021, imx499mipiraw_SWNR_0022, imx499mipiraw_SWNR_0023, imx499mipiraw_SWNR_0024, imx499mipiraw_SWNR_0025, imx499mipiraw_SWNR_0026, imx499mipiraw_SWNR_0027, imx499mipiraw_SWNR_0028, imx499mipiraw_SWNR_0029,
    imx499mipiraw_SWNR_0030, imx499mipiraw_SWNR_0031, imx499mipiraw_SWNR_0032, imx499mipiraw_SWNR_0033, imx499mipiraw_SWNR_0034, imx499mipiraw_SWNR_0035, imx499mipiraw_SWNR_0036, imx499mipiraw_SWNR_0037, imx499mipiraw_SWNR_0038, imx499mipiraw_SWNR_0039,
    imx499mipiraw_SWNR_0040, imx499mipiraw_SWNR_0041, imx499mipiraw_SWNR_0042, imx499mipiraw_SWNR_0043, imx499mipiraw_SWNR_0044, imx499mipiraw_SWNR_0045, imx499mipiraw_SWNR_0046, imx499mipiraw_SWNR_0047, imx499mipiraw_SWNR_0048, imx499mipiraw_SWNR_0049,
    imx499mipiraw_SWNR_0050, imx499mipiraw_SWNR_0051, imx499mipiraw_SWNR_0052, imx499mipiraw_SWNR_0053, imx499mipiraw_SWNR_0054, imx499mipiraw_SWNR_0055, imx499mipiraw_SWNR_0056, imx499mipiraw_SWNR_0057, imx499mipiraw_SWNR_0058, imx499mipiraw_SWNR_0059,
    imx499mipiraw_SWNR_0060, imx499mipiraw_SWNR_0061, imx499mipiraw_SWNR_0062, imx499mipiraw_SWNR_0063, imx499mipiraw_SWNR_0064, imx499mipiraw_SWNR_0065, imx499mipiraw_SWNR_0066, imx499mipiraw_SWNR_0067, imx499mipiraw_SWNR_0068, imx499mipiraw_SWNR_0069,
    imx499mipiraw_SWNR_0070, imx499mipiraw_SWNR_0071, imx499mipiraw_SWNR_0072, imx499mipiraw_SWNR_0073, imx499mipiraw_SWNR_0074, imx499mipiraw_SWNR_0075, imx499mipiraw_SWNR_0076, imx499mipiraw_SWNR_0077, imx499mipiraw_SWNR_0078, imx499mipiraw_SWNR_0079,
    imx499mipiraw_SWNR_0080, imx499mipiraw_SWNR_0081, imx499mipiraw_SWNR_0082, imx499mipiraw_SWNR_0083, imx499mipiraw_SWNR_0084, imx499mipiraw_SWNR_0085, imx499mipiraw_SWNR_0086, imx499mipiraw_SWNR_0087, imx499mipiraw_SWNR_0088, imx499mipiraw_SWNR_0089,
    imx499mipiraw_SWNR_0090, imx499mipiraw_SWNR_0091, imx499mipiraw_SWNR_0092, imx499mipiraw_SWNR_0093, imx499mipiraw_SWNR_0094, imx499mipiraw_SWNR_0095, imx499mipiraw_SWNR_0096, imx499mipiraw_SWNR_0097, imx499mipiraw_SWNR_0098, imx499mipiraw_SWNR_0099,
    imx499mipiraw_SWNR_0100, imx499mipiraw_SWNR_0101, imx499mipiraw_SWNR_0102, imx499mipiraw_SWNR_0103, imx499mipiraw_SWNR_0104, imx499mipiraw_SWNR_0105, imx499mipiraw_SWNR_0106, imx499mipiraw_SWNR_0107, imx499mipiraw_SWNR_0108, imx499mipiraw_SWNR_0109,
    imx499mipiraw_SWNR_0110, imx499mipiraw_SWNR_0111, imx499mipiraw_SWNR_0112, imx499mipiraw_SWNR_0113, imx499mipiraw_SWNR_0114, imx499mipiraw_SWNR_0115, imx499mipiraw_SWNR_0116, imx499mipiraw_SWNR_0117, imx499mipiraw_SWNR_0118, imx499mipiraw_SWNR_0119,
    imx499mipiraw_SWNR_0120, imx499mipiraw_SWNR_0121, imx499mipiraw_SWNR_0122, imx499mipiraw_SWNR_0123, imx499mipiraw_SWNR_0124, imx499mipiraw_SWNR_0125, imx499mipiraw_SWNR_0126, imx499mipiraw_SWNR_0127, imx499mipiraw_SWNR_0128, imx499mipiraw_SWNR_0129,
    imx499mipiraw_SWNR_0130, imx499mipiraw_SWNR_0131, imx499mipiraw_SWNR_0132, imx499mipiraw_SWNR_0133, imx499mipiraw_SWNR_0134, imx499mipiraw_SWNR_0135, imx499mipiraw_SWNR_0136, imx499mipiraw_SWNR_0137, imx499mipiraw_SWNR_0138, imx499mipiraw_SWNR_0139,
    imx499mipiraw_SWNR_0140, imx499mipiraw_SWNR_0141, imx499mipiraw_SWNR_0142, imx499mipiraw_SWNR_0143, imx499mipiraw_SWNR_0144, imx499mipiraw_SWNR_0145, imx499mipiraw_SWNR_0146, imx499mipiraw_SWNR_0147, imx499mipiraw_SWNR_0148, imx499mipiraw_SWNR_0149,
    imx499mipiraw_SWNR_0150, imx499mipiraw_SWNR_0151, imx499mipiraw_SWNR_0152, imx499mipiraw_SWNR_0153, imx499mipiraw_SWNR_0154, imx499mipiraw_SWNR_0155, imx499mipiraw_SWNR_0156, imx499mipiraw_SWNR_0157, imx499mipiraw_SWNR_0158, imx499mipiraw_SWNR_0159,
    imx499mipiraw_SWNR_0160, imx499mipiraw_SWNR_0161, imx499mipiraw_SWNR_0162, imx499mipiraw_SWNR_0163, imx499mipiraw_SWNR_0164, imx499mipiraw_SWNR_0165, imx499mipiraw_SWNR_0166, imx499mipiraw_SWNR_0167, imx499mipiraw_SWNR_0168, imx499mipiraw_SWNR_0169,
    imx499mipiraw_SWNR_0170, imx499mipiraw_SWNR_0171, imx499mipiraw_SWNR_0172, imx499mipiraw_SWNR_0173, imx499mipiraw_SWNR_0174, imx499mipiraw_SWNR_0175, imx499mipiraw_SWNR_0176, imx499mipiraw_SWNR_0177, imx499mipiraw_SWNR_0178, imx499mipiraw_SWNR_0179,
    imx499mipiraw_SWNR_0180, imx499mipiraw_SWNR_0181, imx499mipiraw_SWNR_0182, imx499mipiraw_SWNR_0183, imx499mipiraw_SWNR_0184, imx499mipiraw_SWNR_0185, imx499mipiraw_SWNR_0186, imx499mipiraw_SWNR_0187, imx499mipiraw_SWNR_0188, imx499mipiraw_SWNR_0189,
    imx499mipiraw_SWNR_0190, imx499mipiraw_SWNR_0191, imx499mipiraw_SWNR_0192, imx499mipiraw_SWNR_0193, imx499mipiraw_SWNR_0194, imx499mipiraw_SWNR_0195, imx499mipiraw_SWNR_0196, imx499mipiraw_SWNR_0197, imx499mipiraw_SWNR_0198, imx499mipiraw_SWNR_0199,
    imx499mipiraw_SWNR_0200, imx499mipiraw_SWNR_0201, imx499mipiraw_SWNR_0202, imx499mipiraw_SWNR_0203, imx499mipiraw_SWNR_0204, imx499mipiraw_SWNR_0205, imx499mipiraw_SWNR_0206, imx499mipiraw_SWNR_0207, imx499mipiraw_SWNR_0208, imx499mipiraw_SWNR_0209,
    imx499mipiraw_SWNR_0210, imx499mipiraw_SWNR_0211, imx499mipiraw_SWNR_0212, imx499mipiraw_SWNR_0213, imx499mipiraw_SWNR_0214, imx499mipiraw_SWNR_0215, imx499mipiraw_SWNR_0216, imx499mipiraw_SWNR_0217, imx499mipiraw_SWNR_0218, imx499mipiraw_SWNR_0219,
    imx499mipiraw_SWNR_0220, imx499mipiraw_SWNR_0221, imx499mipiraw_SWNR_0222, imx499mipiraw_SWNR_0223, imx499mipiraw_SWNR_0224, imx499mipiraw_SWNR_0225, imx499mipiraw_SWNR_0226, imx499mipiraw_SWNR_0227, imx499mipiraw_SWNR_0228, imx499mipiraw_SWNR_0229,
    imx499mipiraw_SWNR_0230, imx499mipiraw_SWNR_0231, imx499mipiraw_SWNR_0232, imx499mipiraw_SWNR_0233, imx499mipiraw_SWNR_0234, imx499mipiraw_SWNR_0235, imx499mipiraw_SWNR_0236, imx499mipiraw_SWNR_0237, imx499mipiraw_SWNR_0238, imx499mipiraw_SWNR_0239,
    imx499mipiraw_SWNR_0240, imx499mipiraw_SWNR_0241, imx499mipiraw_SWNR_0242, imx499mipiraw_SWNR_0243, imx499mipiraw_SWNR_0244, imx499mipiraw_SWNR_0245, imx499mipiraw_SWNR_0246, imx499mipiraw_SWNR_0247, imx499mipiraw_SWNR_0248, imx499mipiraw_SWNR_0249,
    imx499mipiraw_SWNR_0250, imx499mipiraw_SWNR_0251, imx499mipiraw_SWNR_0252, imx499mipiraw_SWNR_0253, imx499mipiraw_SWNR_0254, imx499mipiraw_SWNR_0255, imx499mipiraw_SWNR_0256, imx499mipiraw_SWNR_0257, imx499mipiraw_SWNR_0258, imx499mipiraw_SWNR_0259,
    imx499mipiraw_SWNR_0260, imx499mipiraw_SWNR_0261, imx499mipiraw_SWNR_0262, imx499mipiraw_SWNR_0263, imx499mipiraw_SWNR_0264, imx499mipiraw_SWNR_0265, imx499mipiraw_SWNR_0266, imx499mipiraw_SWNR_0267, imx499mipiraw_SWNR_0268, imx499mipiraw_SWNR_0269,
    imx499mipiraw_SWNR_0270, imx499mipiraw_SWNR_0271, imx499mipiraw_SWNR_0272, imx499mipiraw_SWNR_0273, imx499mipiraw_SWNR_0274, imx499mipiraw_SWNR_0275, imx499mipiraw_SWNR_0276, imx499mipiraw_SWNR_0277, imx499mipiraw_SWNR_0278, imx499mipiraw_SWNR_0279,
    imx499mipiraw_SWNR_0280, imx499mipiraw_SWNR_0281, imx499mipiraw_SWNR_0282, imx499mipiraw_SWNR_0283, imx499mipiraw_SWNR_0284, imx499mipiraw_SWNR_0285, imx499mipiraw_SWNR_0286, imx499mipiraw_SWNR_0287, imx499mipiraw_SWNR_0288, imx499mipiraw_SWNR_0289,
    imx499mipiraw_SWNR_0290, imx499mipiraw_SWNR_0291, imx499mipiraw_SWNR_0292, imx499mipiraw_SWNR_0293, imx499mipiraw_SWNR_0294, imx499mipiraw_SWNR_0295, imx499mipiraw_SWNR_0296, imx499mipiraw_SWNR_0297, imx499mipiraw_SWNR_0298, imx499mipiraw_SWNR_0299,
    imx499mipiraw_SWNR_0300, imx499mipiraw_SWNR_0301, imx499mipiraw_SWNR_0302, imx499mipiraw_SWNR_0303, imx499mipiraw_SWNR_0304, imx499mipiraw_SWNR_0305, imx499mipiraw_SWNR_0306, imx499mipiraw_SWNR_0307, imx499mipiraw_SWNR_0308, imx499mipiraw_SWNR_0309,
    imx499mipiraw_SWNR_0310, imx499mipiraw_SWNR_0311, imx499mipiraw_SWNR_0312, imx499mipiraw_SWNR_0313, imx499mipiraw_SWNR_0314, imx499mipiraw_SWNR_0315, imx499mipiraw_SWNR_0316, imx499mipiraw_SWNR_0317, imx499mipiraw_SWNR_0318, imx499mipiraw_SWNR_0319,
    imx499mipiraw_SWNR_0320, imx499mipiraw_SWNR_0321, imx499mipiraw_SWNR_0322, imx499mipiraw_SWNR_0323, imx499mipiraw_SWNR_0324, imx499mipiraw_SWNR_0325, imx499mipiraw_SWNR_0326, imx499mipiraw_SWNR_0327, imx499mipiraw_SWNR_0328, imx499mipiraw_SWNR_0329,
    imx499mipiraw_SWNR_0330, imx499mipiraw_SWNR_0331, imx499mipiraw_SWNR_0332, imx499mipiraw_SWNR_0333, imx499mipiraw_SWNR_0334, imx499mipiraw_SWNR_0335, imx499mipiraw_SWNR_0336, imx499mipiraw_SWNR_0337, imx499mipiraw_SWNR_0338, imx499mipiraw_SWNR_0339,
    imx499mipiraw_SWNR_0340, imx499mipiraw_SWNR_0341, imx499mipiraw_SWNR_0342, imx499mipiraw_SWNR_0343, imx499mipiraw_SWNR_0344, imx499mipiraw_SWNR_0345, imx499mipiraw_SWNR_0346, imx499mipiraw_SWNR_0347, imx499mipiraw_SWNR_0348, imx499mipiraw_SWNR_0349,
    imx499mipiraw_SWNR_0350, imx499mipiraw_SWNR_0351, imx499mipiraw_SWNR_0352, imx499mipiraw_SWNR_0353, imx499mipiraw_SWNR_0354, imx499mipiraw_SWNR_0355, imx499mipiraw_SWNR_0356, imx499mipiraw_SWNR_0357, imx499mipiraw_SWNR_0358, imx499mipiraw_SWNR_0359,
    imx499mipiraw_SWNR_0360, imx499mipiraw_SWNR_0361, imx499mipiraw_SWNR_0362, imx499mipiraw_SWNR_0363, imx499mipiraw_SWNR_0364, imx499mipiraw_SWNR_0365, imx499mipiraw_SWNR_0366, imx499mipiraw_SWNR_0367, imx499mipiraw_SWNR_0368, imx499mipiraw_SWNR_0369,
    imx499mipiraw_SWNR_0370, imx499mipiraw_SWNR_0371, imx499mipiraw_SWNR_0372, imx499mipiraw_SWNR_0373, imx499mipiraw_SWNR_0374, imx499mipiraw_SWNR_0375, imx499mipiraw_SWNR_0376, imx499mipiraw_SWNR_0377, imx499mipiraw_SWNR_0378, imx499mipiraw_SWNR_0379,
    imx499mipiraw_SWNR_0380, imx499mipiraw_SWNR_0381, imx499mipiraw_SWNR_0382, imx499mipiraw_SWNR_0383, imx499mipiraw_SWNR_0384, imx499mipiraw_SWNR_0385, imx499mipiraw_SWNR_0386, imx499mipiraw_SWNR_0387, imx499mipiraw_SWNR_0388, imx499mipiraw_SWNR_0389,
    imx499mipiraw_SWNR_0390, imx499mipiraw_SWNR_0391, imx499mipiraw_SWNR_0392, imx499mipiraw_SWNR_0393, imx499mipiraw_SWNR_0394, imx499mipiraw_SWNR_0395, imx499mipiraw_SWNR_0396, imx499mipiraw_SWNR_0397, imx499mipiraw_SWNR_0398, imx499mipiraw_SWNR_0399,
},
.CA_LTM = {
    imx499mipiraw_CA_LTM_0000, imx499mipiraw_CA_LTM_0001, imx499mipiraw_CA_LTM_0002, imx499mipiraw_CA_LTM_0003, imx499mipiraw_CA_LTM_0004, imx499mipiraw_CA_LTM_0005, imx499mipiraw_CA_LTM_0006, imx499mipiraw_CA_LTM_0007, imx499mipiraw_CA_LTM_0008, imx499mipiraw_CA_LTM_0009,
    imx499mipiraw_CA_LTM_0010, imx499mipiraw_CA_LTM_0011, imx499mipiraw_CA_LTM_0012, imx499mipiraw_CA_LTM_0013, imx499mipiraw_CA_LTM_0014, imx499mipiraw_CA_LTM_0015, imx499mipiraw_CA_LTM_0016, imx499mipiraw_CA_LTM_0017, imx499mipiraw_CA_LTM_0018, imx499mipiraw_CA_LTM_0019,
    imx499mipiraw_CA_LTM_0020, imx499mipiraw_CA_LTM_0021, imx499mipiraw_CA_LTM_0022, imx499mipiraw_CA_LTM_0023, imx499mipiraw_CA_LTM_0024, imx499mipiraw_CA_LTM_0025, imx499mipiraw_CA_LTM_0026, imx499mipiraw_CA_LTM_0027, imx499mipiraw_CA_LTM_0028, imx499mipiraw_CA_LTM_0029,
},
.ClearZoom = {
    imx499mipiraw_ClearZoom_0000, imx499mipiraw_ClearZoom_0001, imx499mipiraw_ClearZoom_0002, imx499mipiraw_ClearZoom_0003, imx499mipiraw_ClearZoom_0004, imx499mipiraw_ClearZoom_0005, imx499mipiraw_ClearZoom_0006,
},
.SWNR_THRES = {
    imx499mipiraw_SWNR_THRES_0000, imx499mipiraw_SWNR_THRES_0001, imx499mipiraw_SWNR_THRES_0002, imx499mipiraw_SWNR_THRES_0003, imx499mipiraw_SWNR_THRES_0004, imx499mipiraw_SWNR_THRES_0005, imx499mipiraw_SWNR_THRES_0006, imx499mipiraw_SWNR_THRES_0007, imx499mipiraw_SWNR_THRES_0008, imx499mipiraw_SWNR_THRES_0009,
    imx499mipiraw_SWNR_THRES_0010, imx499mipiraw_SWNR_THRES_0011, imx499mipiraw_SWNR_THRES_0012, imx499mipiraw_SWNR_THRES_0013, imx499mipiraw_SWNR_THRES_0014, imx499mipiraw_SWNR_THRES_0015, imx499mipiraw_SWNR_THRES_0016, imx499mipiraw_SWNR_THRES_0017, imx499mipiraw_SWNR_THRES_0018, imx499mipiraw_SWNR_THRES_0019,
},
