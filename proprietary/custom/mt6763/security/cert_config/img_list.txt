[single_bin]
logo.bin=logo
boot.img=boot
recovery.img=recovery
md1arm7.img=md1arm7
md1dsp.img=md1dsp
md3img.img=md3rom
spmfw.img=spmfw
sspm.img=tinysys-sspm
odmdtbo.img=dtbo
dtbo.img=dtbo

[multi_bin]
tee.img=atf,atf_dram,tee
lk.img=lk,lk_main_dtb
scp.img=tinysys-loader-CM4_A, tinysys-scp-CM4_A, tinysys-loader-CM4_B, tinysys-scp-CM4_B
md1img.img=md1rom, md1dsp, md1drdi
boot_para.img=dconfig,dconfig-dt
