const ISP_NVRAM_SLK_T SLK_%04d = {
    .rr_con1   ={.bits ={.SLK_R_2=0, .rsv_14=0, .SLK_GAIN_0=0, .SLK_GAIN_1=64}},
    .gain      ={.bits ={.SLK_GAIN_2=128, .SLK_GAIN_3=192, .SLK_GAIN_4=255, .SLK_SET_ZERO=0, .rsv_25=0}},
};