#define IDX_DATA_DM_DIM_NS    6
#define IDX_DATA_DM_FACTOR_SZ    3
#define IDX_DATA_DM_ENTRY_NS    56

static unsigned int _cam_data_entry_DM_key0000[] = {0X0000000C, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_DM_key0001[] = {0X00000020, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_DM_key0002[] = {0X00000000, 0X80200000, 0X00016700, };
static unsigned int _cam_data_entry_DM_key0003[] = {0X00000000, 0X80100000, 0X00016700, };
static unsigned int _cam_data_entry_DM_key0004[] = {0X00000000, 0X80400000, 0X00016700, };
static unsigned int _cam_data_entry_DM_key0005[] = {0X0000000C, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_DM_key0006[] = {0X00000020, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_DM_key0007[] = {0X00000000, 0X40200000, 0X00016700, };
static unsigned int _cam_data_entry_DM_key0008[] = {0X00000000, 0X40100000, 0X00016700, };
static unsigned int _cam_data_entry_DM_key0009[] = {0X00000000, 0X40400000, 0X00016700, };
static unsigned int _cam_data_entry_DM_key0010[] = {0X0000000C, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_DM_key0011[] = {0X00000020, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_DM_key0012[] = {0X00000000, 0X80200000, 0X0001A700, };
static unsigned int _cam_data_entry_DM_key0013[] = {0X00000000, 0X80100000, 0X0001A700, };
static unsigned int _cam_data_entry_DM_key0014[] = {0X00000000, 0X80400000, 0X0001A700, };
static unsigned int _cam_data_entry_DM_key0015[] = {0X0000000C, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_DM_key0016[] = {0X00000020, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_DM_key0017[] = {0X00000000, 0X40200000, 0X0001A700, };
static unsigned int _cam_data_entry_DM_key0018[] = {0X00000000, 0X40100000, 0X0001A700, };
static unsigned int _cam_data_entry_DM_key0019[] = {0X00000000, 0X40400000, 0X0001A700, };
static unsigned int _cam_data_entry_DM_key0020[] = {0X00000001, 0X40040000, 0X000F7D02, };
static unsigned int _cam_data_entry_DM_key0021[] = {0X00000001, 0X80040000, 0X00016500, };
static unsigned int _cam_data_entry_DM_key0022[] = {0X00000001, 0X00040000, 0X000F7D01, };
static unsigned int _cam_data_entry_DM_key0023[] = {0X00000001, 0X40040000, 0X000FBD02, };
static unsigned int _cam_data_entry_DM_key0024[] = {0X00000001, 0X80040000, 0X0001A500, };
static unsigned int _cam_data_entry_DM_key0025[] = {0X00000001, 0X00040000, 0X000FBD01, };
static unsigned int _cam_data_entry_DM_key0026[] = {0X00000002, 0X40080000, 0X000FFD02, };
static unsigned int _cam_data_entry_DM_key0027[] = {0X00000002, 0X00080000, 0X000FFD01, };
static unsigned int _cam_data_entry_DM_key0028[] = {0X00CC0000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_DM_key0029[] = {0X03300000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_DM_key0030[] = {0X6C000000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_DM_key0031[] = {0X00000000, 0XC0000001, 0X000FC7FF, };
static unsigned int _cam_data_entry_DM_key0032[] = {0X00000600, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_DM_key0033[] = {0X00001800, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_DM_key0034[] = {0X0001E000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_DM_key0035[] = {0X0001E00C, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_DM_key0036[] = {0X00000020, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_DM_key0037[] = {0X00000000, 0XC0200000, 0X000FDBFF, };
static unsigned int _cam_data_entry_DM_key0038[] = {0X00000000, 0XC0100000, 0X000FDBFF, };
static unsigned int _cam_data_entry_DM_key0039[] = {0X00000000, 0XC0400000, 0X000FDBFF, };
static unsigned int _cam_data_entry_DM_key0040[] = {0X00000001, 0X80040000, 0X00015900, };
static unsigned int _cam_data_entry_DM_key0041[] = {0X00000001, 0X80040000, 0X00019900, };
static unsigned int _cam_data_entry_DM_key0042[] = {0X00000000, 0XC3800000, 0X000FFFFF, };
static unsigned int _cam_data_entry_DM_key0043[] = {0X0000000C, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_DM_key0044[] = {0X00000020, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_DM_key0045[] = {0X00000000, 0X80200000, 0X0006FF00, };
static unsigned int _cam_data_entry_DM_key0046[] = {0X00000000, 0X80100000, 0X0006FF00, };
static unsigned int _cam_data_entry_DM_key0047[] = {0X00000000, 0X80400000, 0X0006FF00, };
static unsigned int _cam_data_entry_DM_key0048[] = {0X00000001, 0X80040000, 0X0006FF00, };
static unsigned int _cam_data_entry_DM_key0049[] = {0XFFFFFFFF, 0XBFFFFFFF, 0X000FFF00, };
static unsigned int _cam_data_entry_DM_key0050[] = {0X00000020, 0X80000001, 0X000FFF00, };
static unsigned int _cam_data_entry_DM_key0051[] = {0X00000000, 0X80200000, 0X000FFF00, };
static unsigned int _cam_data_entry_DM_key0052[] = {0X00000000, 0X80100000, 0X000FFF00, };
static unsigned int _cam_data_entry_DM_key0053[] = {0X00000000, 0X80400000, 0X000FFF00, };
static unsigned int _cam_data_entry_DM_key0054[] = {0XFFFFFFFF, 0X7FFFFFFF, 0X000FFFF8, };
static unsigned int _cam_data_entry_DM_key0055[] = {0XFFFFFFFF, 0X3FFFFFFF, 0X000FFF07, };

static IDX_MASK_ENTRY _cam_data_entry_DM[IDX_DATA_DM_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_DM_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0001, 8, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0002, 16, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0003, 24, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0004, 32, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0005, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0006, 8, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0007, 16, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0008, 24, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0009, 32, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0010, 40, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0011, 48, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0012, 16, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0013, 24, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0014, 32, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0015, 40, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0016, 48, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0017, 16, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0018, 24, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0019, 32, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0020, 56, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0021, 56, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0022, 64, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0023, 56, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0024, 56, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0025, 64, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0026, 72, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0027, 72, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0028, 56, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0029, 72, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0030, 0, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0031, 8, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0032, 56, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0033, 72, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0034, 0, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0035, 0, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0036, 8, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0037, 16, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0038, 24, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0039, 32, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0040, 56, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0041, 56, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0042, 80, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0043, 0, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0044, 8, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0045, 16, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0046, 24, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0047, 32, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0048, 56, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0049, 0, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0050, 8, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0051, 16, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0052, 24, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0053, 32, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0054, 56, 25, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0055, 72, 26, 0, 0},
};

static unsigned short _cam_data_dims_DM[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_FrontBin,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_Zoom,
};

static unsigned short _cam_data_expand_DM[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_DM =
{
    {IDX_ALGO_MASK, IDX_DATA_DM_DIM_NS, (unsigned short*)&_cam_data_dims_DM, (unsigned short*)&_cam_data_expand_DM},
    {IDX_DATA_DM_ENTRY_NS, IDX_DATA_DM_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_DM}
};